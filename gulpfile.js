var gulp = require('gulp');
var csso = require('gulp-csso');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var ngAnnotate = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var minifyHTML = require('gulp-minify-html');
var gutil = require('gulp-util');
var postcss      = require('gulp-postcss');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer-core');
var path = require('path');
var wrap = require('gulp-wrap');
var babel = require("gulp-babel");
var minify = require('gulp-babel-minify');

gulp.task('autoprefixer', function() {
    gulp.src([
        'style/vendor/*.css',
        'style/components/*.css'
        
    ])
        .pipe(postcss([ autoprefixer({ browsers: ['last 2 version'] }) ]))
        .pipe(gulp.dest('prefixed'));
});

gulp.task('css', function() {
    gulp.src([
        'prefixed/*.css'
    ])
        .pipe(concat('main.min.css'))
        .pipe(csso())
        .pipe(gulp.dest(''));
});



gulp.task('compress', function() {

     gulp.src([
        'js/vendor/*.js',
        'js/views/index.js',
        'js/components/menu-mobile.js',
        'js/components/basket.js',
        'js/components/slider.js',
        'js/components/pagination.js',
        'js/components/map.js'
        
    ])
        .pipe(concat('app.min.js'))
        .pipe(ngAnnotate())
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest(''))
        .on('error', gutil.log);

});

// gulp.task('default', ['autoprefixer']);
// gulp.task('default', ['css', 'compress']);
// gulp.task('default', ['css']);
gulp.task('default', ['compress']);