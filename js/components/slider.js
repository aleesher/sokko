var reviews = ["Lorem Ipsum issimply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
  "Alisher", "Zhangbyrshy"];

var names = ["Бижанов Дулат", "Жолаев Торемурат", "Алжанов Бауыржан", "Муратова Камила"];

var quotes = ['Спасибо ребята,все супер! Сервис отличный, очень вкусно и недорого! Успехов Вам и процветания!!!', 'Заказывали с коллегами два сета на обед. Очень понравилось - всё свежее и вкусное. А ещё, нравится дизайн ваших коробочек :)', 'Спасибо ребята,все супер! Сервис отличный, очень вкусно и недорого! Успехов Вам и процветания!!!', "На день рождение решила разнообразить стол блюдами из вашего меню. Детям понравилась пицца, а взрослые с удовольствием поели суши. Все ушли сытыми и довольными))"];

var commentItems = document.getElementsByClassName("comments-inner--item");
var commentItemNames = document.getElementsByClassName('comments-inner--name');
var commentItemQuotes = document.getElementsByClassName('comments-inner--description');

var commentsInner = document.getElementById("commentsInner");
var length = names.length;
var indx = 0;
var count;

if(window.innerWidth >= 768) {
    count = 2;
} else {
    count = 1;
}


function nextSlide() {
    
        if(count == 2 && indx < length-2) {
            indx+=2;
            commentItemNames[0].innerHTML = names[indx];
            commentItemQuotes[0].innerHTML = quotes[indx];
            commentItemNames[1].innerHTML = names[indx+1];
            commentItemQuotes[1].innerHTML = quotes[indx+1];
        } else if(count == 1 && indx < length-1) {
            indx++;
            commentItemNames[0].innerHTML = names[indx];
            commentItemQuotes[0].innerHTML = quotes[indx];
        }
    
}

function openSlide() {
    if(count == 2) {
        commentItemNames[0].innerHTML = names[0];
        commentItemQuotes[0].innerHTML = quotes[0];
        commentItemNames[1].innerHTML = names[1];
        commentItemQuotes[1].innerHTML = quotes[1];   
    } else {
        commentItemNames[0].innerHTML = names[0];
        commentItemQuotes[0].innerHTML = quotes[0];
    }
}

openSlide();



function prevSlide() {
    if(indx > 0) {
        if(count == 2) {
            indx-=2;
            commentItemNames[0].innerHTML = names[indx];
            commentItemQuotes[0].innerHTML = quotes[indx];
            commentItemNames[1].innerHTML = names[indx+1];
            commentItemQuotes[1].innerHTML = quotes[indx+1];
        } else {
            indx--;
            commentItemNames[0].innerHTML = names[indx];
            commentItemQuotes[0].innerHTML = quotes[indx];
        }
    }
}