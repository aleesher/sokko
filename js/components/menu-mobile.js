var isMenuMobile = false;

function toggleMenu() {
    if(!isMenuMobile) {
        document.getElementById('menu-mobile').style.display = 'block';
        isMenuMobile =! isMenuMobile;
        document.getElementById('menu-dropdown--caret').className = 'menu-dropdown--caret menu-dropdown--caret__open';
        // document.getElementById('body').style.overflowY = 'hidden';
    } else {
        document.getElementById('menu-mobile').style.display = 'none';
        document.getElementById('menu-dropdown--caret').className = 'menu-dropdown--caret';  
        isMenuMobile =! isMenuMobile;
        // document.getElementById('body').style.overflowY = 'auto'; 
    }
}