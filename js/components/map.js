var input_map = document.getElementById('delivery-address');
var btn_map = document.getElementById('delivery-btn');

var coords = [];
var myMap, myMap2;
// var searchAddress;
var circle, circle1, circle2, circle3, circle4;
var myPlacemark, myPlacemark1, myPlacemark2, myPlacemark3, myPlacemark4, myPlacemark5;
var searchControl2;
var draggableObject;
var firstGeoObject;
var delivery;

function init() {
        myMap = new ymaps.Map("map", {
            center: [51.177313, 71.477806],
            zoom: 9,
            controls: []
        });
       
            circle = new ymaps.Circle([[51.177313, 71.477806], 8000],  {
                balloonContent: "Бесплатная доставка <br>Режим работы с 10:00 - 23:30",
                hintContent: "Бесплатная доставка"
            }, { 
                draggable: false,
                fillColor: "#ed190b",
                strokeColor: "#ed190b",
                strokeWidth: 2,
                strokeOpacity: 0.6,
                zIndex: 50,
                fillOpacity: 0.3
            });
             circle1 = new ymaps.Circle([[51.177313, 71.477806], 11000],  {
                balloonContent: "Доплата за доставку в размере 300тг <br>Режим работы с 10:00 - 23:30",
                hintContent: "Доплата за доставку в размере 300тг"
            }, { 
                draggable: false,
                fillColor: "#34a852",
                strokeColor: "#34a852",
                strokeWidth: 2,
                strokeOpacity: 0.6,
                zIndex: 40,
                fillOpacity: 0.3,
                strokeOpacity: 0
            });
            circle2 = new ymaps.Circle([[51.177313, 71.477806], 15000], {
                balloonContent: "Доплата за доставку в размере 500тг<br>Режим работы с 10:00 - 23:30",
                hintContent: "Доплата за доставку в размере 500тг"
            }, { 
                draggable: false,
                fillColor: "#ccbdde",
                strokeColor: "#ccbdde",
                strokeWidth: 2,
                strokeOpacity: 0.6,
                zIndex: 39,
                fillOpacity: 0.3,
                strokeOpacity: 0
            });
            circle3 = new ymaps.Circle([[51.177313, 71.477806], 18000], {
                balloonContent: "Доплата за доставку в размере 700тг<br>Режим работы с 10:00 - 23:30",
                hintContent: "Доплата за доставку в размере 700тг"
            }, { 
                draggable: false,
                fillColor: "#ffc107",
                strokeColor: "#ffc107",
                strokeWidth: 2,
                strokeOpacity: 0.6,
                zIndex: 38,
                fillOpacity: 0.3,
                strokeOpacity: 0
            });
            circle4 = new ymaps.Circle([[51.177313, 71.477806], 22000], {
                balloonContent: "Доплата за доставку договорная <br>Режим работы с 10:00 - 23:30",
                hintContent: "Доплата за доставку договорная"
            }, { 
                draggable: false,
                fillColor: "#03a9f4",
                strokeColor: "#03a9f4",
                strokeWidth: 2,
                strokeOpacity: 0.6,
                zIndex: 37,
                fillOpacity: 0.3,
                strokeOpacity: 0
            });

    myMap.geoObjects.add(circle);
    myMap.geoObjects.add(circle1);
    myMap.geoObjects.add(circle2);
    myMap.geoObjects.add(circle3);
    myMap.geoObjects.add(circle4);
    
    myPlacemark = new ymaps.Placemark([51.177313, 71.477806], {
        balloonContent: 'Бесплатная доставка<br>Режим работы с 10:00 - 23:30'
    }, {
        balloonPanelMaxMapArea: 0
    });
    myPlacemark2 = new ymaps.Placemark([51.136498, 71.363451], {
        balloonContent: 'Доплата за доставку в размере 300тг<br>Режим работы с 10:00 - 23:30'
    }, {
        balloonPanelMaxMapArea: 0
    });
    myPlacemark3 = new ymaps.Placemark([51.217381, 71.633035], {
        balloonContent: 'Доплата за доставку в размере 500тг<br>Режим работы с 10:00 - 23:30'
    }, {
        balloonPanelMaxMapArea: 0
    });
    myPlacemark4 = new ymaps.Placemark([51.276512, 71.646501], {
        balloonContent: 'Доплата за доставку в размере 700тг<br>Режим работы с 10:00 - 23:30'
    }, {
        balloonPanelMaxMapArea: 0
    });
    myPlacemark5 = new ymaps.Placemark([51.078233, 71.227518], {
        balloonContent: 'Доплата за доставку договорная<br>Режим работы с 10:00 - 23:30'
    }, {
        balloonPanelMaxMapArea: 0
    });
    
    myMap.geoObjects.add(myPlacemark);
    myMap.geoObjects.add(myPlacemark2);
    myMap.geoObjects.add(myPlacemark3);
    myMap.geoObjects.add(myPlacemark4);
    myMap.geoObjects.add(myPlacemark5);
    
    
    searchControl = new ymaps.control.SearchControl({
        options: {
            provider: 'yandex#search',
            noSelect: true
        }
    });

    
    myMap.controls.add(searchControl);
    
    myPlacemark.balloon.open();
    
    myPlacemark.balloon.close();

    observeEvents(myMap);
    
}


function observeEvents (map) {
    var mapEventsGroup;
    map.geoObjects.each(function (geoObject) {
        geoObject.balloon.events
            // При открытии балуна начинаем слушать изменение центра карты.
            .add('open', function (e1) {
                var placemark = e1.get('target');
                // Вызываем функцию в двух случаях:
                mapEventsGroup = map.events.group()
                    // 1) в начале движения (если балун во внешнем контейнере);
                    .add('actiontick', function (e2) {
                        if (placemark.options.get('balloonPane') == 'outerBalloon') {
                            setBalloonPane(map, placemark, e2.get('tick'));
                        }
                    })
                    // 2) в конце движения (если балун во внутреннем контейнере).
                    .add('actiontickcomplete', function (e2) {
                        if (placemark.options.get('balloonPane') != 'outerBalloon') {
                            setBalloonPane(map, placemark, e2.get('tick'));
                        }
                    });
                // Вызываем функцию сразу после открытия.
                setBalloonPane(map, placemark);
            })
            // При закрытии балуна удаляем слушатели.
            .add('close', function () {
                mapEventsGroup.removeAll();
            });
    });
}


function setBalloonPane (map, placemark, mapData) {
    mapData = mapData || {
        globalPixelCenter: map.getGlobalPixelCenter(),
        zoom: map.getZoom()
    };

    var mapSize = map.container.getSize(),
        mapBounds = [
            [mapData.globalPixelCenter[0] - mapSize[0] / 2, mapData.globalPixelCenter[1] - mapSize[1] / 2],
            [mapData.globalPixelCenter[0] + mapSize[0] / 2, mapData.globalPixelCenter[1] + mapSize[1] / 2]
        ],
        balloonPosition = placemark.balloon.getPosition(),
    // Используется при изменении зума.
        zoomFactor = Math.pow(2, mapData.zoom - map.getZoom()),
    // Определяем, попадает ли точка привязки балуна в видимую область карты.
        pointInBounds = ymaps.util.pixelBounds.containsPoint(mapBounds, [
            balloonPosition[0] * zoomFactor,
            balloonPosition[1] * zoomFactor
        ]),
        isInOutersPane = placemark.options.get('balloonPane') == 'outerBalloon';

    // Если точка привязки не попадает в видимую область карты, переносим балун во внутренний контейнер
    if (!pointInBounds && isInOutersPane) {
        placemark.options.set({
            balloonPane: 'balloon',
            balloonShadowPane: 'shadows'
        });
        // и наоборот.
    } else if (pointInBounds && !isInOutersPane) {
        placemark.options.set({
            balloonPane: 'outerBalloon',
            balloonShadowPane: 'outerBalloon'
        });
    }
}


function init2() {
    myMap2 = new ymaps.Map("map2", {
        center: [51.177313, 71.477806],
        zoom: 9
    });

    myMap2.geoObjects.add(circle);
    myMap2.geoObjects.add(circle1);
    myMap2.geoObjects.add(circle2);
    myMap2.geoObjects.add(circle3);
    myMap2.geoObjects.add(circle4);

   draggableObject = new ymaps.GeoObject({
            // The geometry description.
            geometry: {
                type: "Point",
                coordinates: [51.177313, 71.477806]
            },
            // Properties.
            properties: {
                // The placemark content.
                iconContent: 'Я',
                hintContent: 'Перенесите на ваш адрес.'
            }
        }, {
            /**
             * Options.
             * The placemark's icon will stretch to fit its contents.
             */
            // preset: 'islands#blackStretchyIcon',
            strokeColor: 'rgba(52, 168, 83, .8)',
            // The placemark can be dragged.
            draggable: true
        });

    // myMap2.geoObjects.add(myPlacemark);
    // myMap2.geoObjects.add(myPlacemark2);
    // myMap2.geoObjects.add(myPlacemark3);
    // myMap2.geoObjects.add(myPlacemark4);
    // myMap2.geoObjects.add(myPlacemark5);
    myMap2.geoObjects.add(draggableObject);

    searchControl = new ymaps.control.SearchControl({
        options: {
            provider: 'yandex#search',
            noSelect: true
        }
    });

    myMap.controls.add(searchControl);

    myPlacemark.balloon.open();
    
    myPlacemark.balloon.close();
    
    observeEvents(myMap);

}
ymaps.ready(init);
ymaps.ready(init2);

function showBasketMap(action) {
    if(action == 1) {
        document.getElementsByClassName('basket-map')[0].style.display = 'block';
        
    } else {
        document.getElementsByClassName('basket-map')[0].style.display = 'none';
    }
}

function setAddress() {

    draggableObject.properties.set('iconCaption', 'Поиск...');
    ymaps.geocode(draggableObject.geometry._coordinates).then(function(res){
        firstGeoObject = res.geoObjects.get(0);
        draggableObject.properties.set('iconCaption', firstGeoObject.getAddressLine());
        document.getElementById('show-map-btn').innerHTML = 'изменить адрес';
        document.getElementById('selected-adress').innerHTML = firstGeoObject.getAddressLine();    
        

        var objects = ymaps.geoQuery([
            {
                type: 'Point',
                coordinates: draggableObject.geometry._coordinates
            }
        ]).addToMap(myMap2);

        if(objects.searchInside(circle)._objects.length > 0) {

            delivery = 0;

        } else if(objects.searchInside(circle1)._objects.length > 0) {

            delivery = 300;
            
        } else if(objects.searchInside(circle2)._objects.length > 0) {

            delivery = 500;

        } else if(objects.searchInside(circle3)._objects.length > 0) {

            delivery = 700;
            
        } else if(objects.searchInside(circle4)._objects.length > 0) {
            
            delivery = "договорная";
        }

        if(typeof(delivery) == 'number') {
            var price = total_price + delivery;
            document.getElementById('basket-second-delivery').innerHTML = 'Доставка: ' + delivery + ' тнг';
            document.getElementById('basket-first-totalPrice').innerHTML = 'Итого: ' + (total_price + delivery) + ' тнг';
            showBasketMap('0');
        }else {
            showBasketMap('0');
            document.getElementById('basket-second-delivery').innerHTML = 'Доставка: ' + delivery;
            document.getElementById('basket-first-totalPrice').innerHTML = 'Итого: ' + total_price + ', доставка ' + delivery;
            
        }   
        
        ymaps.geoQuery(objects).removeFromMap(myMap2);
        draggableObject.properties.iconCaption = '';

        // console.log(draggableObject.properties);
    });

}
                    
function noAddress() {
    delivery = 'договорная';
    showBasketMap('0');
    document.getElementById('basket-second-delivery').innerHTML = 'Доставка: ' + delivery;
    document.getElementById('basket-first-totalPrice').innerHTML = 'Итого: ' + total_price + ', доставка ' + delivery;
}