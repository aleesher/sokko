function toggleBasket (action) {
    if(action=='show') {
    	var basket = JSON.parse(localStorage.getItem('basket'));
    	
    	if(basket) {
    		basket_list = '';
    		total_price = 0;
    		if(basket.length > 0) {
    		    basket_list+='<div class="basket-first-list" id="basket-first-list-rerender">';
    		   
                           
                        
    			basket.forEach(function(item, index){
    			    
    			basket_list +=  
    			'<div class="basket-first-item">' +
                    '<img class="basket-first-item--close" src="images/widgets/basket-cancel.svg" onclick="deleteFromBasket('+item.id+')">'+
                    '<div class="basket-first-item--img">' +
                    	'<img src="' + item.image+'">' +
                    '</div>' +
                    '<div class="basket-first-item-info">' +
                    	'<div>' + 
                    		'<span class="basket-first-item--title">' +
                    			item.title + ' (' + item.portion + ')' +
                    		'</span>' +
                    		'<span class="basket-first-item--subtitle">' +
                    			item.composition +
                    		'</span>' +
                    	'</div>' +
                    	'<div class="basket-first-item-action">' +
                    		'<div class="basket-first-item--counter">' +
                    			'<span class="basket-first-item--decrement" onclick="setCounterBasket(0,'+ item.id + ')">-</span>' +
                    			'<span class="basket-first-item--amount" id="basket-first-item--amount-' + item.id+ '">' +
                    				item.amount + 
                    			'</span>' +
                                '<span class="basket-first-item--increment" onclick="setCounterBasket(1,'+ item.id + ')">+</span>' +
                            '</div>' +
                            '<div class="basket-first-item--price" id="basket-first-item--price-' + item.id+ '">' +
                            	item.price + ' тнг' +
                            '</div>' +
                        '</div>' +
                    '</div>' + 
                '</div>';
                total_price += item.price;
    		});
    		
    		basket_list+='</div>'+
                        
                        '<div class="basket-first-buy" id="basket-first-buy">'+
                         '<span class="basket-first--total-price" id="basket-first-total">Сумма: 2850 тнг</span>'+
                            '<button class="basket-first--btn-buy" onclick="toggleBasketTwo(\'show\')">купить</button>'+
                        '</div>'+
                        '<div class="basket-slider">'+
                            '<div>'+
                                '<i class="active">1</i>'+
                                '<span>Корзина</span>'+
                            '</div>'+
                            '<div>'+
                                '<i>2</i>'+
                                '<span>Детали</span>'+
                            '</div>'+
                            '<div>'+
                                '<i>3</i>'+
                                '<span> Итог</span>'+
                            '</div>'+
                        '</div>';

    		document.getElementById('basket-first-list').innerHTML = basket_list;		
    		document.getElementById('basket-first-total').innerHTML = 'Сумма: ' + total_price + ' тнг';

    		} else {
    		    document.getElementById('basket-first-list').innerHTML = '<span class="basket-empty" id="basket-empty">Ваша корзина пуста</span>';
    			document.getElementById('basket-empty').style.display = 'block';
    			
    		}
    		
    	} else {
    	    document.getElementById('basket-first-list').innerHTML = '<span class="basket-empty" id="basket-empty">Ваша корзина пуста</span>';
    		document.getElementById('basket-empty').style.display = 'block';
    	}
    	

        document.getElementById('basket').style.display = 'block';
        document.getElementById('body').style.overflowY = 'hidden';
    } else {
        document.getElementById('basket').style.display = 'none';
        document.getElementById('body').style.overflowY = 'auto';
        checkBasketItems();
    }
}

function toggleBasketTwo(action){
    console.log(action);
    if(action=='show') {
        basket_html='';
        total_price = 0;
        var basket = JSON.parse(localStorage.getItem('basket'));
        if(basket) {
            basket_html+=
                 '<div class="basket-second-title">'+
                           '<span>Пожалуйста, заполните поля</span>'+
                           '<img src="images/logo-black.png">'+
                       '</div>'+
                       '<form id="form" onsubmit="toggleBasketThree(event, \'last\')">'+
                       '<fieldset class="form-field">'+
                           '<label>Имя</label>'+
                           '<input type="text" placeholder="Введите Ваше имя" required id="name"/>'+
                       '</fieldset>'+
                       '<fieldset class="form-field">'+
                           '<label>Номер телефона</label>'+
                           '<input type="text" placeholder="+7 ___ ___ __ __" required minlength="11"  id="phone"/>'+
                       '</fieldset>'+
                       '<fieldset class="form-field">'+

                            '<label>Адрес</label>'+
                            '<div id="selected-adress" class="selected-adress">Не выбран</div>'+
                       '</fieldset>'+
                       '<fieldset class="form-field">'+
                            '<div id="show-map-btn" class="basket-first--btn-buy confirm" onclick="showBasketMap(1)">выбрать адрес</div>'+
                       '</fieldset>'+
                       '<fieldset class="form-field">'+
                           '<label>Квартира</label>'+
                           '<input type="text" placeholder="Номер квартиры" maxlength="10" id="room"/>'+
                       '</fieldset>'+
                       '<fieldset class="form-field">'+
                           '<label>Квартира/Офис</label>'+
                           '<input type="text" placeholder="Квартира 3"  id="flat"/>'+
                       '</fieldset>'+
                      '<div class="basket-products">'+
                            '<h2>Корзина товаров</h2>';
                            
                            basket.forEach(function(item, index){
                                total_price += item.price;
                                basket_html+=
                                '<div class="basket-products-item">'+
                                    '<span>-  '+item.title + ' (' + item.portion + ')' +'</span> <span>'+item.amount+' шт.</span> <span>'+item.price+' Т</span>'+
                                '</div>';
                            });
                            
                            basket_html+=
                            '<div class="basket-first-buy" id="basket-first-buy">'+
                                '<span class="basket-first--total-price" id="basket-second-delivery">Стоимость доставки: Адрес не выбран</span>'+
                                '<span class="basket-first--total-price" id="basket-first-totalPrice">Сумма: 2850 тнг</span>'+
                                '<button class="basket-first--btn-buy" type="submit">отправить</button>'+
                            '</div>'+
                            
                       '</div>'+
                       '</form>'+
                        '<div class="basket-slider">'+
                            '<div>'+
                                '<i>1</i>'+
                                '<span>Корзина</span>'+
                            '</div>'+
                            '<div>'+
                                '<i class="active">2</i>'+
                                '<span> Детали</span>'+
                            '</div>'+
                            '<div>'+
                                '<i>3</i>'+
                                '<span>    Итог</span>'+
                            '</div>'+
                        '</div>';
                       
                    document.getElementById('basket-first-list').innerHTML = basket_html;		
    		        document.getElementById('basket-first-total').innerHTML = 'Сумма: ' + total_price + ' тнг';
                   // document.getElementById('basket-first-delivery').innerHTML = 'Цена за доставку: ' + delivery + ' тнг';
                    document.getElementById('basket-first-totalPrice').innerHTML = 'Итого: ' + total_price + ' тнг';
                    // ymaps.ready(init2);
        }
    } 
}



function deleteFromBasket(id){
    var basket = JSON.parse(localStorage.getItem('basket'));
    
    var find_index = basket.findIndex(function(element) {
        return element.id == id
    });
    
    basket.splice(find_index, 1);
    localStorage.setItem('basket', JSON.stringify(basket));
    if(document.getElementById('product-'+id))
        document.getElementById('product-'+id).style.bottom = '-40px';
    
    toggleBasket('show');
    
    countBasket();
}

function setCounterBasket (action, id) {
    var amount = document.getElementById('basket-first-item--amount-'+id);
    var price = document.getElementById('basket-first-item--price-'+id);
    var basket = JSON.parse(localStorage.getItem('basket'));

    var find_index = basket.findIndex(function(element) {
        return element.id == id
    });

    if(action==1) {
        
        
        basket[find_index].price += (basket[find_index].price/basket[find_index].amount);
        basket[find_index].amount ++;
        localStorage.setItem('basket', JSON.stringify(basket));
        
        amount.innerHTML = basket[find_index].amount;
        price.innerHTML = basket[find_index].price;
        total_price+=basket[find_index].price/basket[find_index].amount;
        document.getElementById('basket-first-total').innerHTML = 'Сумма: ' + total_price + ' тнг';   
    } else {

        if(amount.innerHTML == '1') {
            deleteFromBasket(id);
            
        } else {

            
            basket[find_index].price -= (basket[find_index].price/basket[find_index].amount);
            basket[find_index].amount --;
            localStorage.setItem('basket', JSON.stringify(basket));
            amount.innerHTML = basket[find_index].amount;
            price.innerHTML = basket[find_index].price;
            total_price-=basket[find_index].price/basket[find_index].amount;
            document.getElementById('basket-first-total').innerHTML = 'Сумма: ' + total_price + ' тнг';
        }

    } 
    
    countBasket();
}


function countBasket(){
    var basket = JSON.parse(localStorage.getItem('basket'));
    if(basket) {
        document.getElementById("count-basket").innerHTML = basket.length;
        document.getElementById("count-basket-mobile").innerHTML = basket.length;
    } else {
        document.getElementById("count-basket").innerHTML = "0";
        document.getElementById("count-basket-mobile").innerHTML = "0";
    }
    
}
function toggleBasketThree(event, action){

    event.preventDefault();
    if(action=='last') {
        var basket = JSON.parse(localStorage.getItem('basket'));
        if(basket){

            var total = document.getElementById('basket-first-totalPrice').innerHTML;
            total = total.substring(7, total.length);
            var data = { 
                items: basket,
                name: document.getElementById("name").value,
                address: document.getElementById('selected-adress').innerHTML,
                phone: document.getElementById("phone").value,
                total: total,
                room: document.getElementById('room').value
            }
            axios.post(base_url+'api/order.php', data)
            .then(function(response) {
                console.log(response);
            })
            .catch(function(err){
                console.log(err);


            })
                            console.log(data)

            
            basket_html = 
                '<div class="basket-third">'+
                    '<img src="images/car.png">'+
                    '<span>'+
                        'Спасибо за покупку!<br>'+
                        'Ожидайте заказ в течение 60-90 минут.<br>'+
                        'Приятного аппетита!'+
                    '</span>'+
                '</div>'+
                '<div class="basket-slider">'+
                    '<div>'+
                        '<i>1</i>'+
                        '<span>Корзина</span>'+
                    '</div>'+
                    '<div>'+
                        '<i>2</i>'+
                        '<span>Детали</span>'+
                    '</div>'+
                    '<div>'+
                        '<i class="active">3</i>'+
                        '<span>    Итог</span>'+
                    '</div>'+
                '</div>';
            
            document.getElementById('basket-first-list').innerHTML = basket_html;
            
            
                
            
            
            localStorage.removeItem("basket");
            getProducts(0);
            
            
        }
         
    }
}