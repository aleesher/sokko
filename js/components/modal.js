function showModal(check) {
    var modal = document.getElementById('modal');
    if(check == 'show') {
        modal.style.display = "block";
    } else {
        modal.style.display = "none";
    }
}

function showModalEdit(check) {
    var modalEdit = document.getElementById('modal-edit');
    if(check == 'show') {
        modalEdit.style.display = "block";
    } else {
        modalEdit.style.display = "none";
    }
}
