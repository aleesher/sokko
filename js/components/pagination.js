var currentPage = 0;
var previous = 0;
var pagination = document.getElementById("pagination");
var pagItems = document.getElementsByClassName('pagination--item');
var pagLen = 7;
var currentPagination = 0;
var menu = 'menu';
getProducts(currentPage);
function nextPage() {
    if(currentPage != getSize() - 1) {
        currentPage++;
        getProducts(currentPage, 'next');
    }
}

function openPage(indx) {
    currentPage = indx;
    getProducts(currentPage, 'next');
}


function prevPage() {
    // console.log(currentPage);
    if(currentPage != 0) {
        currentPage--;
        getProducts(currentPage, 'prev');
    }
}

function setActive(indx) {
    // console.log(indx);
    if(indx > currentPagination)
        pagItems[indx-currentPagination].className = "pagination--item active";
    else
        pagItems[indx].className = "pagination--item active";
}

function setArrowDisabled(indx) {
    getLeftArrow().className = "pagination--item img left";
    getRightArrow().className = "pagination--item img";
    if(indx == 0) {
        getLeftArrow().className = "pagination--item img left disabled";
    } 
    if(indx == getSize() - 1) {
        getRightArrow().className = "pagination--item img disabled";
    }
}

function setPagination(size, page, indx, option) {
    console.log(size, page, indx, option)
    if(page == 'admin') {
        page = './';
    } else {
        page = './';
    }
    // console.log("size: " + getSize());
    var str = "";
    
    if(getSize() > 0 && getSize() != undefined && getSize() != null) {
        // console.log('indx: '+indx);
        // console.log(pagLen -indx)
        currentPage = indx;
        if(option == undefined || option == 'filter') {
            if(getSize() < 7)  {
                pagLen = getSize();
                currentPagination = 0;
            } else {
                pagLen = 7;
                currentPagination = 0;
            }
        }

        if(indx > pagLen-1 && option == 'next') {
            if(pagLen+7 > getSize()) {
                pagLen = getSize();
                currentPagination = indx;
            } else {
                pagLen += 7;
                currentPagination = indx;
            }
            // console.log('next')
        }
        
        if(option == 'prev' && indx == currentPagination-1) {
            if(getSize() == pagLen && pagLen % 7 == 0) {
                pagLen -= 7;
                currentPagination = indx-6;
            } else if(getSize() == pagLen && pagLen % 7 != 0) {
                pagLen = pagLen - (pagLen % 7);
                currentPagination = indx - (pagLen % 7) - 6;
            } else {
                pagLen -=7;
                currentPagination = indx-6;
            }
            if(getSize() <= 7)  {
                pagLen = getSize();
                currentPagination = 0;
            } else if(pagLen <= 7) {
                pagLen = 7;
                currentPagination = 0;
            }
        }
        
        
        str += '<div class="pagination--item img left" id="left-arrow" onclick="prevPage();  scrollToB(menu, 1);">' +
                   '<img src="' + page + './images/widgets/right.svg"/>'+
               '</div>';
        for(var i = currentPagination; i < pagLen; i++) {
            str += '<div class="pagination--item" onclick="openPage(' + i + '); scrollToB(menu, 1);">' +
                    (i+1) +
            '</div>';
            
        }
        
        str +=  '<div class="pagination--item img" id="right-arrow" onclick="nextPage();  scrollToB(menu, 1);">'+
                    '<img src="' + page + './images/widgets/right.svg"/>'+
                '</div>';
    }
    pagination.innerHTML = str;
    if(str != "") {
        return true;
    } else {
        return false;
    }
}