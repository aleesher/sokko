var isBackdrop = false;
var isAside = false;
var leftArrow;
var rightArrow
var size;
var filter;



if(!localStorage.getItem('filterProduct')) {
    filter = 'all';
    localStorage.setItem('filterProduct', filter);
} else {
    filter = localStorage.getItem('filterProduct');
}

function getProducts(page, option) {
	axios.get(base_url+'api/admin/get.php?page=' + page + '& filter=' + filter)
	.then(function(response) {
	    console.log(response.data[0]);
		getCarts(response.data[0]);
		setSize(response.data[1]);
		var check = setPagination(getSize(), 'index', page, option);
		if(check == true) {
			setLeftArrow(document.getElementById("left-arrow"));
			setRightArrow(document.getElementById("right-arrow"));
			setArrowDisabled(page);
			setActive(page+1);
		}
	})
	.catch(function(error) {
		console.log(error);
	})	
}

function setSize(size) {
	this.size = size;
}

function getSize() {
	return this.size;
}

function setLeftArrow(arrow) {
	this.leftArrow = arrow;
}

function getLeftArrow() {
	return this.leftArrow;
}

function setRightArrow(arrow) {
	this.rightArrow = arrow;
}

function getRightArrow() {
	return this.rightArrow;
}


function addToCart (id) {
    document.getElementById('product-' + id).style.bottom = '0px';
    showMyBasket('show','plus');
    setTimeout(function() {showMyBasket('hide')}, 4000);
    var amount = document.getElementById('product-amount-' + id).innerHTML = 1;
    var title = document.getElementById('product-title-'+id).innerHTML;
    var portion = document.getElementById('product-portion-'+id).innerHTML;
    var price = parseInt(document.getElementById('product-price-'+id).innerHTML.split(' ')[0]);
    var composition = document.getElementById('product-composition-'+id).innerHTML;
    var image = document.getElementById('product-image-'+id).getAttribute('src');
    
    var basket = JSON.parse(localStorage.getItem('basket'));
    var find_index = -1;

    var new_item = {
        id: parseInt(id),
        title: title,
        portion: portion,
        price: price,
        composition: composition,
        amount: amount,
        image: image
    }

    if(basket) {

        find_index = basket.findIndex(function(element){
            return element.id == id 
        });
            
            if(find_index == -1) {

                basket.unshift(new_item);
                localStorage.setItem('basket', JSON.stringify(basket));

                // basket[find_index].amount = amount.innerHTML + 1;
                // basket[find_index].price =+ price;
                // localStorage.setItem('basket', JSON.stringify(basket));

            }

    } else {

        basket = [new_item];
        localStorage.setItem('basket', JSON.stringify(basket));        

    }
    countBasket();
}

function setCounter (action, id) {
    var amount = document.getElementById('product-amount-'+id);
    var price = parseInt(document.getElementById('product-price-'+id).innerHTML.split(' ')[0])
    
    var basket = JSON.parse(localStorage.getItem('basket'));

    var find_index = basket.findIndex(function(element) {
        return element.id == id
    });

    if(action==1) {
        showMyBasket('show', 'plus');
        setTimeout(function() {showMyBasket('hide', 'plus')}, 3000);
        basket[find_index].amount ++;
        basket[find_index].price += price;
        localStorage.setItem('basket', JSON.stringify(basket));
        amount.innerHTML = basket[find_index].amount;
            
    } else {
        showMyBasket('show', 'minus');
        setTimeout(function() {showMyBasket('hide', 'minus')}, 3000);
        
        if(amount.innerHTML == '1') {

            basket.splice(find_index, 1);
            localStorage.setItem('basket', JSON.stringify(basket));
            document.getElementById('product-'+id).style.bottom = '-40px'; 

        } else {

            basket[find_index].amount --;
            basket[find_index].price -= price;
            localStorage.setItem('basket', JSON.stringify(basket));
            amount.innerHTML = basket[find_index].amount;

        }

    } 
    countBasket();
}

function toggleAside (action){
    if(!isAside) {
        isAside = true;
        document.getElementById('aside').style.left = '0';
        document.getElementById('backdrop').style.display = 'block';
        document.getElementById('body').style.overflowY = 'hidden';
    } else {
        isAside = false;
        document.getElementById('aside').style.left = '-75%';
        document.getElementById('backdrop').style.display = 'none';   
        document.getElementById('body').style.overflowY = 'auto';
    }
}

function getCarts(data) {
    var menuCard = document.getElementById("menu-card");

    var str = "";
    for(var i = 0; i < data.length; i++) {
        str += '<div class="product-item">' +
                    '<div class="product-item--image">' +
                        '<img src="./' + data[i].img.substring(5, data[i].img.length) + '" id="product-image-' + data[i].id +'">' +
                    '</div>' +
                    '<div class="product-item-info">' +
                        '<span class="product-item--title" id="product-title-' +data[i].id+'">' + data[i].name + '</span>'+
                        '<span class="product-item--portion" id="product-portion-' +data[i].id+'">' + data[i].count + '</span>'+
                        '<span class="product-item--price" id="product-price-' +data[i].id+'">' + data[i].price + ' ₸' + '</span>'+
                        '<span class="product-item--composition" id="product-composition-' +data[i].id+'">' + data[i].ingredients + '</span>'+

                    '</div>'+
                    '<div class="product-item--button" onclick="addToCart('+ data[i].id+')">'+
                            '<img src="images/bask.png">'+
                            'добавить в корзину'+
                    '</div>'+
                    '<div class="product-item--counter" id="product-' + data[i].id + '">' + 
                        '<button class="product-item--decrement" onclick="setCounter(0,'+ data[i].id + ')">-</button>'+
                        '<span class="product-item--amount" id="product-amount-' + data[i].id+ '">1</span>'+
                        '<button class="product-item--increment" onclick="setCounter(1,'+ data[i].id + ')">+</button>'+
                    '</div>'+
                '</div>'
    }
    
    menuCard.innerHTML = str;
    checkBasketItems();
}

function filterCategory(fltr) {
    filter = fltr;
    localStorage.setItem('filterProduct', fltr);
    getProducts(0);
    
    if(isMenuMobile) {
        isMenuMobile =! isMenuMobile;
        document.getElementById('menu-mobile').style.display = 'none';
        document.getElementById('menu-dropdown--caret').className = 'menu-dropdown--caret';
        scroll.toElement(document.getElementById('menu')).then(function () {
            
        }); 
    }
    
}

    


function checkBasketItems() {
    var basket = JSON.parse(localStorage.getItem('basket'));
    if(basket) {
        for(var i = 0; i < basket.length; i++){
            if(document.getElementById('product-amount-' + basket[i].id)) {
                document.getElementById('product-amount-'+ basket[i].id).innerHTML = basket[i].amount;
                document.getElementById('product-'+basket[i].id).style.bottom = '0px';
                console.log('here');
            }
        }
    }
    countBasket();
}

var scroll = new Scroll(document.body);

function scrollToB(id, index) {
    console.log(id, index);
    var element = document.getElementById(id);
    var nav_list = document.getElementsByClassName('nav-list');
    for (var i = 0; i < nav_list.length; i++) {
        nav_list[i].className = 'nav-list';
    }

    nav_list[index].className = 'nav-list nav-list__active';
        
    scroll.toElement(element).then(function () {
    });    
}

function asideScrollTo(id ,index) {
    var element = document.getElementById(id);
    var aside_list = document.getElementsByClassName('aside-list');
    isAside = false;
    document.getElementById('aside').style.left = '-75%';
    document.getElementById('backdrop').style.display = 'none';   
    document.getElementById('body').style.overflowY = 'auto';    

    for (var i = 0; i < aside_list.length; i++) {
        aside_list[i].className = 'aside-list';
    }


    scroll.toElement(element, {easing: 'easeInOutCubic', duration: 700}).then(function () {
        aside_list[index].className = 'aside-list aside-list__active';
    });


}

function showMyBasket(option, action) {
    var element1 = document.getElementById('mybasket');
    var element2 = document.getElementById('mybasket2');
    element1.style.display = 'none';
    element2.style.display = 'none';
    
    if(option == 'show' && action == 'plus') {
        element1.style.display = 'flex';
    } else if(option == 'show' && action == 'minus') {
        element2.style.display = 'flex';
    }

}