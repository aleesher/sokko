var inputs = document.getElementsByClassName("input");
var txtarea = document.getElementsByClassName("textarea");
var menuTableRow = document.getElementsByClassName("menu-table--row");
var productItems = document.getElementsByClassName("product-item");
var editImgs = document.getElementsByClassName('row--item img');
var menuTable = document.getElementById('menu-table');
var menuCard = document.getElementById('menu-card');
var selects = document.getElementsByClassName("select admin__slct");
var selecteds = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"];
var leftArrow;
var rightArrow;
var back = "back";
var size;
var filter = "";
var categoryFilter = document.getElementById("categoryFilter");
// var categories = ["Все", "Суши" , "Роллы", "Теплые роллы", "Запеченные роллы", "Сеты", "Салаты", "Супы", "Закуски", "Пицца", "Горячие блюда", "Гарниры", "Десерты", "Напитки", "Хосомаки"];


if(!localStorage.getItem("filter")) {
	filter = "all";
	localStorage.setItem("filter", filter);
} else {
	filter = localStorage.getItem("filter");
}

categoryFilter.value = filter;


// function getProducts(page, option) {
// 	axios.get('/sokko/api/admin/get.php?page=' + page + '&filter=' + localStorage.getItem("filter"))

function getProducts(page, option) {
	axios.get(base_url+'api/admin/get.php?page=' + page + '&filter=' + localStorage.getItem("filter"))
	.then(function(response) {
		console.log(response.data[0]);
		getTable(response, 0);
		getCart(response.data[0]);
		setSize(response.data[1]);
		
		var check = setPagination(getSize(), 'admin', page, option);
		if(check == true) {
			setLeftArrow(document.getElementById("left-arrow"));
			setRightArrow(document.getElementById("right-arrow"));
			setArrowDisabled(page);
			setActive(page+1);
		}
	})
	.catch(function(error) {
		console.log(error);
	})	
}

function setSize(size) {
	this.size = size;
}

function getSize() {
	return this.size;
}

function setLeftArrow(arrow) {
	this.leftArrow = arrow;
}

function getLeftArrow() {
	return this.leftArrow;
}

function setRightArrow(arrow) {
	this.rightArrow = arrow;
}

function getRightArrow() {
	return this.rightArrow;
}



function editRow(indx) {
	show(indx, true);

}

function edited(indx, id) {
	if(id == 'back') {
		show(indx, false);	
	} else {
		var e = document.getElementById("category" + id);
		var category= e.options[e.selectedIndex].value;
	
		axios.post(base_url+'api/admin/edit.php?id=' + id + '&option=table', {
			name: document.getElementById('name' + id).value,
			count: document.getElementById('count'+ id).value,
			ingredients: document.getElementById('ingredients'+ id).value,
			price: document.getElementById('price'+ id).value,
			categoryId: category
		})
		.then(function(response) {
			console.log(response);
			getProducts(0);
		}).catch(function(error) {
			console.log(error);
		});
		show(indx, false);
	}
	
}

function show(id, check) {

	var indx = id * 4;
	
	if(check == true) {
		
		txtarea[id*1-1].className = "textarea admin__txt active";
		selects[id*1-1].className = "select admin__slct active";

		editImgs[indx-4].style.display = "none";
		editImgs[indx-3].style.display = "none";
		editImgs[indx-2].style.display = "block";
		editImgs[indx-1].style.display = "block";

		var myId = (id-1)*3;
	
		for(var i = myId; i < myId+3; i++) {
			inputs[i].className = "input admin__inp active";
		}
	} else {
		
		txtarea[id*1-1].className = "textarea admin__txt";
		selects[id*1-1].className = "select admin__slct";
		editImgs[indx-4].style.display = "block";
		editImgs[indx-3].style.display = "block";
		editImgs[indx-2].style.display = "none";
		editImgs[indx-1].style.display = "none";
		
		var myId = (id-1)*3;
	
		for(var i = myId; i < myId+3; i++) {
			inputs[i].className = "input admin__inp";
			
		}
	}
}
//should be edited
function deleteRow(indx,id) {
	axios.delete(base_url+'api/admin/delete.php?id=' + id)
	.then(function(response) {
		productItems[indx-1].style.display = "none";
		menuTableRow[indx].style.display = "none";
	}).catch(function(error) {
		console.log(error);
	})
}




function displayNow(option) {
	if(option == 'table') {
		document.getElementById('menu-card').style.display = 'none';
		document.getElementById('menu-table').style.display = 'block';
	} else {
		document.getElementById('menu-table').style.display = 'none';
		document.getElementById('menu-card').style.display = 'flex';
	}
}

displayNow('card');


function deleteCart(indx, id) {
	axios.delete(base_url+'api/admin/delete.php?id=' + id)
	.then(function(response) {
		productItems[indx-1].style.display = "none";
		menuTableRow[indx].style.display = "none";
	}).catch(function(error) {
		console.log(error);
	})
}

function editCart(indx,id, name, count, ingredients, price, categoryId, img, order1, order2) {
	var currentInputs = [id, name, count, ingredients, price, categoryId, order1, order2];
	var editInputs = document.getElementsByClassName("modal--edit--input");
	
	for(var i = 0; i < editInputs.length; i++) {
		editInputs[i].value = currentInputs[i];
	}
	
	var myEditImg = document.getElementById("myEditImg");
	myEditImg.src = './' + img.substring(5, img.length);
	
	showModalEdit('show');

}
function uploadFileUrlG(file) {
	if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	var img = document.getElementById("myEditImg");
        	img.src = e.target.result;
        	console.log(img.src);
        };
        reader.readAsDataURL(file.files[0]);
    }
}

function getCart(data) {
	var str = "";
	// console.log(data)
	for(var i = 0; i < data.length; i++) {
		
		str += '<div class="product-item">' +
			        '<div class="product-item--image">' +
			            '<img src="./' + data[i].img.substring(5, data[i].img.length) + '">' +
			        '</div>' +
			        '<div class="product-item-info">' +
			            '<span class="product-item--title">' + data[i].name + '</span>' +
			            '<span class="product-item--portion">' + data[i].count + ' </span>' +
			            '<span class="product-item--price">' + data[i].price + ' ₸' + '</span>' +
			            '<span class="product-item--composition">' + data[i].ingredients + '</span>' +
			        '</div>' +
			        '<div class="product-item--edit first" onclick="editCart(' + (i+1) + ", " + data[i].id + ", '" + data[i].name + "', '" + data[i].count + "', '" + data[i].ingredients + "', '" + data[i].price + "', '" + data[i].category_id + "', '" + data[i].img + "', '" + data[i].order1 + "', '" + data[i].order2 + "'" + ')">' +
			            '<img src="images/widgets/pencil.svg"/>' +
			        '</div>' +
			        '<div class="product-item--edit" onclick="deleteCart(' + (i+1) + ", " + data[i].id + ')">' +
			            '<img src="images/widgets/remove.svg"/>' +
			        '</div>'+
			    '</div>'
	}
	menuCard.innerHTML = str;
 	
}

function getTable(response, page) {
	var str = '<div class="menu-table--row first">' +
                    '<div class="row--item">' +
                        'Название' +
                    '</div>' +
                    '<div class="row--item">' +
                        'Количество' +
                    '</div>' +
                    '<div class="row--item">' +
                        'Ингредиенты' +
                    '</div>' +
                    '<div class="row--item">' +
                        'Цена' +
                    '</div>' +
                    '<div class="row--item">' +
                        'Категория' +
                    '</div>' + 
                '</div>';
	for(var i = 0; i < response.data[0].length; i++) {
		str += "<div class='menu-table--row'>" +
                    "<div class='row--item'>" +
                       "<input type='text' id='name" + response.data[page][i].id + "' class='input admin__inp' value='" + response.data[page][i].name + "'/>" +
                    "</div>" +
                    "<div class='row--item'>" + 
                        "<input type='text' id='count" + response.data[page][i].id + "' class='input admin__inp' value='" + response.data[page][i].count + "'/>" + 
                    "</div>" +
                    "<div class='row--item'>" +
                        "<textarea class='textarea admin__txt' id='ingredients" + response.data[page][i].id + "'>" + response.data[page][i].ingredients + "</textarea>" + 
                    "</div>" + 
                    "<div class='row--item'>" + 
                        "<input type='number' id='price" + response.data[page][i].id + "' class='input admin__inp' value='" + response.data[page][i].price + "'/>" + 
                    "</div>" +
                    "<div class='row--item'>" + 
                         '<select class="select admin__slct" id="category' + response.data[page][i].id + '">' +
                        	'<option value="1">Суши</option>' +
                        	'<option value="14">Хосомаки</option>' +
                        	'<option value="2">Роллы</option>' +
                        	'<option value="3">Теплые роллы</option>' +
                        	'<option value="4">Запечённые роллы</option>' +
                        	'<option value="5">Сеты</option>' +
                        	'<option value="6">Салаты</option>' +
                        	'<option value="7">Супы</option>' +
                        	'<option value="8">Закуски</option>' +
                        	'<option value="9">Пицца</option>' +
                        	'<option value="10">Горячие блюда</option>' +
                        	'<option value="11">Гарниры</option>' +
                        	'<option value="12">Десерты</option>' +
                        	'<option value="13">Напитки</option>' +
                         '</select>' +
                    "</div>" + 
                    "<div class='row--item img' onclick='editRow(" + (i+1) + ")'>" + 
                        "<img src='images/widgets/pencil.svg'/>" + 
                    "</div>" +
                    "<div class='row--item img last' onclick='deleteRow(" + (i+1) + ", " + response.data[page][i].id + ")'>" + 
                        "<img src='images/widgets/remove.svg'/>" + 
                    "</div>" + 
                    "<div class='row--item img ok' onclick='edited(" + (i+1) + ", " + response.data[page][i].id + ")'>" +
                        'OK' +
                    "</div>" +
                    "<div class='row--item img cancel' onclick='edited(" + (i+1) + ", " + back + ");'>" +
                        'Назад' +
                    "</div>" +
                "</div>"
                
                var id = "category"+ response.data[page][i].id;
                var category_id =  response.data[page][i].category_id;
                setTimeout(function() {
					
				}, 0.1);
	}
	
	menuTable.innerHTML = str;
	
	for(var i = 0; i < response.data[0].length; i++) {
		var id = "category"+ response.data[page][i].id;
        var category_id =  response.data[page][i].category_id;
        document.getElementById(id).value = selecteds[category_id-1];
	}
}

function filterCategory() {
	var selectedCategory = document.getElementById('categoryFilter');
	filter = selectedCategory.options[selectedCategory.selectedIndex].value;
	console.log(localStorage.getItem("filter"));
	localStorage.setItem("filter", filter);
	getProducts(0, 'filter');
}