<?php 

    session_start();
    if(isset($_SESSION["login"]) && isset($_SESSION["pass"])) {
        
    // session_destroy();
?>
<html>
    <head>
        <title>Админ панель  | Sokko Food</title>
        <meta name="fragment" content="!">
        <meta name="robots" content="index, follow">
        <meta content="" name="description">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="stylesheet" href="style/all.css" type="text/css"/>-->
        <link rel="stylesheet" href="main.min.css" type="text/css"/>
    </head>
    <body>
        <?php 
            include('views/components/modal.php');
            include('views/components/modal-edit.php');
        ?>
    	<section class="admin">
             <div class="admin--title">Админ панель</div>   
             <div class="admin-menu" id="admin-menu">
                 <div class="admin-menu--add">
                     <button class="btn admin__add" onclick="showModal('show')">
                         Добавить
                     </button>
                 </div>
                 <div class="admin-menu--view" onclick="displayNow('card');">
                    <img src="./images/widgets/menu-card.svg"/>
                 </div>
                 <div class="admin-menu--view first" onclick="displayNow('table');">
                    <img src="./images/widgets/menu-list.svg"/>
                 </div>
                <div class="admin-menu--select">
                    <select class="select admin__select" id="categoryFilter" onchange="filterCategory()">
                        <option value="all">Все</option>
                        <option value="1">Суши</option>
                        <option value="14">Хосомаки</option>
                    	<option value="2">Роллы</option>
                    	<option value="3">Теплые роллы</option>
                    	<option value="4">Запечённые роллы</option>
                    	<option value="5">Сеты</option>
                    	<option value="6">Салаты</option>
                    	<option value="7">Супы</option>
                    	<option value="8">Закуски</option>
                    	<option value="9">Пицца</option>
                    	<option value="10">Горячие блюда</option>
                    	<option value="11">Гарниры</option>
                    	<option value="12">Десерты</option>
                    	<option value="13">Напитки</option>
                    </select>
                </div>
                <!--<div class="admin-menu--filter">-->
                <!--    <button class="btn admin__filter" onclick="filterCategory()">-->
                <!--         Применить-->
                <!--     </button>-->
                <!--</div>-->
                <div class="menu-table" id="menu-table">
                </div>
                <div class="admin-menu--card">
                    <div class="menu-products full__admin" id="menu-card">
                    </div>
                </div>
                 <?php 
                    include "views/components/pagination.php";
                 ?>
             </div>
        </section>
        <script src="js/vendor/axios.min.js"></script>
        <script src="js/views/admin.js"></script>
        <script src="js/components/modal.js"></script>
        <script src="js/components/pagination.js"></script>
    </body>
</html>

<?php 
    } else {
      header("Location: views/pages/login");
    }
?>

