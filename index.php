<!DOCTYPE html>
<html>
    <head>
        <meta name="fragment" content="!">
        <meta name="robots" content="index, follow">
        <meta content="" name="description">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="stylesheet" href="style/all.css" type="text/css"/>-->
        <link rel="stylesheet" href="main.min.css" type="text/css"/>
    </head>
    <body id="body">
        

        <?php
            include ('views/components/header.php');
            include ('views/components/aside.php');
            include ('views/components/menu-mobile.php');
        ?>

        <section class="basket" id="basket">
            <div class="basket-inner">
                <div class="basket-backdrop" onclick="toggleBasket('hide')"></div>
                <div class="basket-wrap">
                    <div class="basket-first" id="basket-first-list">
                      

                    </div>
                    <span class="basket-close" onclick="toggleBasket('hide')">Закрыть</span>
                    
                </div>               
            </div>
        </section>

        <?php
            include ('views/components/main.php');
            include ('views/components/mybasket.php');
            include ('views/components/menu.php');
            // include ('views/components/video.php');
            include ('views/components/flow.php');
            include ('views/components/delivery.php');
            include ('views/components/refs.php');
            include ('views/components/comments.php');
            include ('views/components/seo-text.php');
            include ('views/components/footer.php');
            include ('views/components/backdrop.php');
            include ('views/components/floating-button.php');
        ?>
        <div class="basket-map">
            <div class="basket-map-inner" id="map2">
                <img class="basket-map__close" src="images/close-button.svg" onclick="showBasketMap(0)">
                <button class="basket-map__confirm" onclick="setAddress()">да, это мой адрес</button>
                <button class="basket-map__confirm basket-map__confirm--undefined">нет моего адреса</button>
            </div>
        </div>
    </body>
    

    <script type="text/javascript" src="js/vendor/axios.min.js"></script>
    <script type="text/javascript" src="js/vendor/scroll-min.js"></script>
    <script type="text/javascript" src="js/views/index.js"></script>
    <script type="text/javascript" src="js/components/menu-mobile.js"></script>
    <script type="text/javascript" src="js/components/basket.js"></script>
    <script type="text/javascript" src="js/components/slider.js"></script>
    <script type="text/javascript" src="js/components/pagination.js"></script>
    
    <!-- map -->
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <!--<script type="text/javascript" src="js/components/map.js"></script>-->

    <!--<script type="text/javascript" src="app.min.js"></script>-->
</html>



