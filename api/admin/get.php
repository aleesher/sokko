<?php 
    include "../db.php";
    
    $page = intval($_GET['page']);
    $filter = $_GET['filter'];
    $page = $page*6;
    $page2 = 6;
    
    
    if($filter == 'all') {
        $sql = "SELECT * FROM product ORDER BY order2 LIMIT " . $page . ", " . $page2;
    } else {
        $sql = "SELECT * FROM product WHERE category_id=" . $filter . " ORDER BY order1 LIMIT ".$page.", ".$page2;
    }
    $result = $conn->query($sql);
    $array = array();
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            array_push($array, $row);
        }
    }
    if($filter == 'all') {
        $sql = "SELECT COUNT(*) AS size FROM product";
    } else {
        $sql = "SELECT COUNT(*) AS size FROM product WHERE category_id=".$filter;
    }
    $result = $conn->query($sql);
    if($row = $result->fetch_assoc()) {
       $size = $row["size"];
    }
    $size = ceil($size/6);
    
    echo json_encode(array($array, $size, $page, $page2, $filter));
?>