<?php 
    include "../db.php";
    include "../crop.php";
    
    if(isset($_POST['name']) && isset($_POST['count']) && isset($_POST['price'])
       && isset($_POST['ingredients']) && isset($_POST['category'])) {
        
          $query = $conn->query("SELECT MAX(id) AS size FROM product");
          $row=$query->fetch_object();
          $add=$row->size+1;
    	  $temp = explode(".",$_FILES["img"]["name"]);
    		  
    	  $source_img =  "../../images/old_products/product"."_".$add.'.'.end($temp);
    	  move_uploaded_file($_FILES['img']['tmp_name'], $source_img);
          $destination_img = "../../images/products/product"."_".$add.'.'.end($temp);
          $d = compress($source_img, $destination_img, 70);
          move_uploaded_file($_FILES['img']['tmp_name'], $d);
          unlink($source_img);
          
    	  $name = $_POST['name'];
          $count = $_POST['count'];
      	  $price = $_POST['price'];
      	  $ingredients = $_POST['ingredients'];
    	  $categoryId = $_POST['category'];
    	  $order1 = $_POST['order1'];
    	  $order2 = $_POST['order2'];
    		  
          $sql = "INSERT INTO product(id, name, count, ingredients, price, img, date, category_id, order1, order2)
                  VALUES(NULL,\"".$name."\",\"".$count."\", \"".$ingredients."\", \"".$price."\", 
                  \"".$d."\", \""."NOW()"."\",\"".$categoryId."\",\"".$order1."\",\"".$order2."\")";
                  
          $conn->query($sql);
          
          header('Location: /admin');
          
      } else {
          header('Location: /admin');
          // echo "Error";
      }
?>