<?php
require '../PHPMailer-master/PHPMailerAutoload.php';

$mail = new PHPMailer;

// прием отправленных данных с ангуляр
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$name = $request->name;
$phone = $request->phone;

$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';                 // Specify main and backup server
$mail->Port = 587;                                    // Set the SMTP port
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'sokkofood@gmail.com';                // SMTP username
$mail->Password = 'sf2017sf';                  // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
$mail->CharSet = 'UTF-8';

$mail->From = 'sokkofood@gmail.com';
$mail->FromName = $request->name;
$mail->AddAddress('sokkofood@gmail.com', 'Sokko Food');  // Add a recipient

$mail->IsHTML(true);                                  // Set email format to HTML

$forsend_html= "Новый заказ <br>" . 
             "Имя: ".$request->name . "<br>" . 
             "Телефон: ".$request->phone. "<br>".
             "<table>".
                "<tr>".
                    "<td>Наименование</td>".
                    "<td>Кол-во</td>".
                    "<td>Цена за ед.</td>".
                    "<td>Итого</td>".
                "<tr>";

$total = 0;
for($i = 0; $i<sizeof($request->items); $i++) {
    $item_cost = $request->items[$i]->price / $request->items[$i]->amount;
    $total += $request->items[$i]->price;
    $forsend_html .= "<tr>".
                        "<td>". $request->items[$i]->title ." (".$request->items[$i]->portion.")</td>".
                        "<td>".$request->items[$i]->amount ."</td>".
                        "<td>".$item_cost ."</td>".
                        "<td>". $request->items[$i]->price."</td>".
                    "</tr>";
}

$forsend_html .= "<tr>".
                        "<td></td>".
                        "<td></td>".
                        "<td>Сумма</td>".
                        "<td>". $total ."</td>".
                    "</tr>";

$forsend_html .= "</table>";

$mail->Subject = "Заказ на sokkofood.kz от " . $request->name; 
$mail->Body    = $forsend_html;
                    
// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->Send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   echo $forsend_html."Новый заказ <br>" . $request->items;
   exit;
} else {
    echo $forsend_html;
}



