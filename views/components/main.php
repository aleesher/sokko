<section class="main" id="main">
    <div class="main-inner">
        <img class="main--logo" src="images/logo-white.png">
        <h1 class="main--title">Ресторан вкусного питания <br> <span>с доставкой</span> </h1>
        <h2 class="main--subtitle">Мы запускаем уникальный проект, где вы можете попробывать <br> изысканные блюда за демократичные цены не выходя из дома</h2>
        <h2 class="main--subtitle__mobile">Доставка еды, от которой вы будете в восторге</h2>
        
        <span class="main--callback" onclick="scrollToB('menu', 1)">попробовать еду</span>
    </div>
    <div class="main--bg">
        <img src="images/main-bg.png">                
    </div>
</section>