<header class="header">
    <div class="header-mobile">
        <img class="header-menu" src="images/menu-button.svg" onclick="toggleAside()">                    
        <div class="menu-dropdown" onclick="toggleMenu()">
            наше меню
            <img class="menu-dropdown--caret" src="images/caret-down.svg" id="menu-dropdown--caret">
        </div>
        <div class="header-mobile-basket">
            <div>
                <span class="header-mobile-basket-counter" id="count-basket-mobile">0</span>
                <img src="images/bask.png" onclick="toggleBasket('show')">                    
            </div>
        </div>
    </div>
    <div class="logo">
        <img class="logo-img" src="images/logo-1-min.png">
    </div>
    <div class="nav">
        <li class="nav-list nav-list__active" onclick="scrollToB('main', 0)">Главная</li>
        <li class="nav-list" onclick="scrollToB('menu', 1)">Меню</li>
        <li class="nav-list" onclick="scrollToB('flow', 2)">О нас</li>
        <li class="nav-list" onclick="scrollToB('delivery', 3)">Доставка</li>
        <li class="nav-list" onclick="scrollToB('footer', 4)">Отзывы</li>
        <li class="nav-list"><a href="tel:+77714079997"></a>+7 771 407 99 97</li>
    </div>
    <div class="header-basket">
        <div class="header-basket-inner">
            <span class="header-basket-counter" id="count-basket">0</span>
            <img src="images/bask.png" onclick="toggleBasket('show')">                    
        </div>
    </div>
</header>