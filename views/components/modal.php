<section class="modal" id="modal">
    <img class="modal--close" src="./images/close-button.svg" onclick="showModal('hide')">
    <form action="/api/admin/add.php" method="post" class="modal-inner" enctype="multipart/form-data">
        <span class="modal--title">Добавить новый продукт</span>
        <input class="modal--input" type="text" name="name" id="name" placeholder="Название">
        <input class="modal--input" type="text" name="count" id="count" placeholder="Количество">
        <input class="modal--input" type="text" name="ingredients" id="ingredients" placeholder="Ингредиенты">
        <input class="modal--input" type="number" name="price" id="price" placeholder="Цена">
        <select class="modal--input" name="category" id="category">
            <option value="1">Суши</option>
            <option value="14">Хосомаки</option>
        	<option value="2">Роллы</option>
        	<option value="3">Теплые роллы</option>
        	<option value="4">Запечённые роллы</option>
        	<option value="5">Сеты</option>
        	<option value="6">Салаты</option>
        	<option value="7">Супы</option>
        	<option value="8">Закуски</option>
        	<option value="9">Пицца</option>
        	<option value="10">Горячие блюда</option>
        	<option value="11">Гарниры</option>
        	<option value="12">Десерты</option>
        	<option value="13">Напитки</option>
        </select>
        <input  class="modal--input" type="number" id="order1" name="order1" placeholder="Сортировка в своей категории"/>
        <input class="modal--input" type="number" id="order2" name="order2" placeholder="Сортировка в общей категории"/>
        <input class="modal--input" type="file" name="img" id ="img">
        
        <button type="submit" class="btn btn__modal" onclick="showModal('hide');">Добавить</button>
    </form>
</section>