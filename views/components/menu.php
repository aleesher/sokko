<section class="menu" id="menu">
            <span class="menu-title">Меню</span>
            <span class="menu-subtitle">Bon Appétit</span>
            <div class="menu-inner">
                <div class="menu-list">
                    <li class="menu-list--item" onclick="filterCategory('all')">Все</li>
                    <li class="menu-list--item" onclick="filterCategory('1')">Суши</li>
                    <li class="menu-list--item" onclick="filterCategory('14')">Хосомаки</li>
                    <li class="menu-list--item" onclick="filterCategory('2')">Роллы</li>
                    <li class="menu-list--item" onclick="filterCategory('3')">Теплые роллы</li>
                    <li class="menu-list--item" onclick="filterCategory('4')">Запеченные роллы</li>
                    <li class="menu-list--item" onclick="filterCategory('5')">Сеты</li>
                    <li class="menu-list--item" onclick="filterCategory('6')">Салаты</li>
                    <li class="menu-list--item" onclick="filterCategory('7')">Супы</li>
                    <li class="menu-list--item" onclick="filterCategory('8')">Закуски</li>
                    <li class="menu-list--item" onclick="filterCategory('9')">Пицца</li>
                    <li class="menu-list--item" onclick="filterCategory('10')">Горячие блюда</li>
                    <li class="menu-list--item" onclick="filterCategory('11')">Гарниры</li>
                    <li class="menu-list--item" onclick="filterCategory('12')">Десерты</li>
                    <li class="menu-list--item" onclick="filterCategory('13')">Напитки</li>
                     
                    
                    
                    <!--<li class="menu-list--item">-->
                        <!--корзина-->
                        <!-- <img src="images/bask.png"> -->
                    <!--</li>-->
                    <!--<li class="menu-list--item menu-list--item__link">-->
                    <!--    <a href="#">Скачать полное меню</a>-->
                    <!--</li> -->
                </div>
                <div class="menu-products" id="menu-card">
                    <div class="product-item">
                        <div class="product-item--image">
                            <img src="images/products/PAI_0209-min.jpg" id="product-image-1">
                        </div>
                        <div class="product-item-info">
                            <span class="product-item--title" id="product-title-1">калифорния суши</span>
                            <span class="product-item--portion" id="product-portion-1">16 роллов</span>
                            <span class="product-item--price" id="product-price-1">15000 тнг</span>
                            <span class="product-item--composition" id="product-composition-1">Теплый ролл с копченым угрем и сливочным сыром Филадельфия</span>
                        </div>
                        <div class="product-item--button" onclick="addToCart('1')">
                                <img src="images/bask.png">
                                добавить в корзину
                        </div>
                        <div class="product-item--counter" id="product-1">
                            <button class="product-item--decrement" onclick="setCounter('decrement', 1)">-</button>
                            <span class="product-item--amount" id="product-amount-1">1</span>
                            <button class="product-item--increment" onclick="setCounter('increment', 1)">+</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                include ('pagination.php');
            ?>
        </section>