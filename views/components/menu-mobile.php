<section class="menu-mobile" id="menu-mobile">
	<div class="menu-mobile-list" style="position: relative; overflow-y: auto; max-height: calc(100vh - 60px)">
		<li class="menu-mobile-list--item" onclick="filterCategory('all')">Все</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('1')">Суши</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('14')">Хосомаки</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('2')">Роллы</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('3')">Теплые роллы</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('4')">Запеченные роллы</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('5')">Сеты</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('6')">Салаты</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('7')">Супы</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('8')">Закуски</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('9')">Пицца</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('10')">Горячие блюда</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('11')">Гарниры</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('12')">Десерты</li>
        <li class="menu-mobile-list--item" onclick="filterCategory('13')">Напитки</li>
	</div>
</section>