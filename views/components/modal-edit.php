<section class="modal" id="modal-edit" style="overflow-y: auto;">
    <img class="modal--close" src="./images/close-button.svg" onclick="showModalEdit('hide')">
    <form action="/api/admin/edit-modal.php" method="post" class="modal-inner" enctype="multipart/form-data" style="margin-top: 0%;">
        <span class="modal--title">Изменить продукт</span>
        <input type="hiddden" style="display: none;" name="id" class="modal--edit--input" value=""/>
        <input class="modal--edit--input" type="text" name="name" id="name" placeholder="Название" value="">
        <input class="modal--edit--input"  type="text" name="count" id="count" placeholder="Количество" value="">
        <input class="modal--edit--input" type="text" name="ingredients" id="ingredients" placeholder="Ингредиенты" value="">
        <input class="modal--edit--input"  type="number" name="price" id="price" placeholder="Цена" value="">
        <select class="modal--edit--input" name="category" id="categoryEdit">
            <option value="1">Суши</option>
            <option value="14">Хосомаки</option>
        	<option value="2">Роллы</option>
        	<option value="3">Теплые роллы</option>
        	<option value="4">Запечённые роллы</option>
        	<option value="5">Сеты</option>
        	<option value="6">Салаты</option>
        	<option value="7">Супы</option>
        	<option value="8">Закуски</option>
        	<option value="9">Пицца</option>
        	<option value="10">Горячие блюда</option>
        	<option value="11">Гарниры</option>
        	<option value="12">Десерты</option>
        	<option value="13">Напитки</option>
        </select>
        <input  class="modal--edit--input" type="number" id="order1" name="order1" placeholder="Сортировка в своей категории"/>
        <input class="modal--edit--input" type="number" id="order2" name="order2" placeholder="Сортировка в общей категории"/>
        <input type="file" name="img" id ="img" value="" onchange="uploadFileUrlG(this)">
        <div class="modal--edit--img">
            <img id="myEditImg" src=""/>
        </div>
        <button type="submit" class="btn btn__modal" onclick="showModalEdit('hide');">Подтвердить</button>
    </form>
</section>