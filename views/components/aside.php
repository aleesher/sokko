<div class="aside" id="aside">
    <div class="aside-inner">
        <img class="aside-logo" src="images/logo-1-min.png">
        <li class="aside-list" onclick="asideScrollTo('main', 0)">Главная</li>
        <li class="aside-list" onclick="asideScrollTo('flow', 1)">О нас</li>
        <li class="aside-list" onclick="asideScrollTo('delivery', 2)">Доставка</li>
        <li class="aside-list" onclick="asideScrollTo('comments', 3)">Отзывы</li>
        <div class="aside-contacts">
            <span class="aside-contacts--title">Sokko Food - первый ресторан в Астане с вкусной едой и доставкой</span>
            <span class="aside-contacts--phone"> <a href="tel:+77172727266">+7 (717) 272 72 66</a>; <a href="tel:+77714079997">+7 771 407 99 97</a></span>
            <span class="aside-contacts--address">www.sokkofood.kz</span>
        </div>
    </div>
</div>