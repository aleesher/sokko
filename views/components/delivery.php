<div class="delivery" id="delivery">
    <span class="delivery--title">Регионы Доставки</span>
    <!-- <input type="text" name="address" id="delivery-address" placeholder="проспект Сарыарка"> -->
    <!-- <button class="btn" id="delivery-btn">Подтвердить</button> -->
    <script type="text/javascript">
        
    </script>
    <div class="delivery-inner">
        <div class="delivery-info">
            <span class="delivery-info--title">Прайс на доставку</span>
            <div class="delivery-content">
                <span class="delivery-content--circle circle__first">
                </span>
                <div class="delivery-content--title">
                    Доставка бесплатна <br>
                    в пределах этого района
                </div>    
            </div>
            <div class="delivery-content">
                <span class="delivery-content--circle circle__second">
                </span>
                <div class="delivery-content--title">
                    Доплата за доставку <br>
                    в размере 300тг
                </div>    
            </div>
            <div class="delivery-content">
                <span class="delivery-content--circle circle__third">
                </span>
                <div class="delivery-content--title">
                    Доплата за доставку <br>
                    в размере 500тг
                </div>    
            </div>
            <div class="delivery-content">
                <span class="delivery-content--circle circle__fourth">
                </span>
                <div class="delivery-content--title">
                    Доплата за доставку <br>
                    в размере 700тг
                </div>    
            </div>
            <div class="delivery-content">
                <span class="delivery-content--circle circle__fifth">
                </span>
                <div class="delivery-content--title">
                    Доплата за доставку <br>
                    договорная
                </div>    
            </div>
        </div>
        <div class="delivery-map">
            <!--<iframe src="https://yandex.com/map-widget/v1/?um=constructor%3A983579aad0cd59e7a468362e0652120434f097568188128fc279c929b2ace97e&amp;lang=en_US&amp;scroll=true&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>-->
            <div id="map" style="width: 100%; height: 100%;"></div>
        </div>
        <!--<div style="width: 50%; height: 400px; padding: 0; margin: 0;" id="map">-->
                <!--<script type="text/javascript" charset="utf-8"  src="../js/components/myMap.js"></script>-->
                
        <!--</div>-->
    </div>
</div>
