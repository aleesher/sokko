<section class="block" id="refs">
            <div class="refs">
                <div class="refs--title">
                    Давайте дружить
                </div>
                <div class="refs--grid">
                    <div class="refs--grid--item">
                        <span class="item--text web">
                            <a href="https://www.facebook.com/sokko.food/?fref=ts" target="_blank">facebook.com</a>
                        </span>
                        <span class="item--img">
                            <img src="images/widgets/facebook.svg"/>    
                        </span>
                    </div>
                    <div class="refs--grid--item">
                        <span class="item--text telegram">
                            <a href="https://vk.com/id452172309" target="_blank">vk.com</a>
                        </span>
                        <span class="item--img">
                            <img src="images/widgets/vk.svg"/>    
                        </span>
                    </div>
                    <div class="refs--grid--item">
                        <span class="item--text gmail">
                            <a href="https://www.instagram.com/sokko.food/" target="_blank">instagram</a>
                        </span>
                        <span class="item--img">
                            <img src="images/widgets/instagram.svg"/>    
                        </span>
                    </div>
                </div>
            </div>
        </section>