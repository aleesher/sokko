<section class="flow" id="flow">
    <h1 class="flow--title">Мы ценим Ваше время и аппетит</h1>
    <h2 class="flow--subtitle">Покупая еду в Sokko Food, Вы всегда останетесь довольны</h2>
    <div class="flow-inner">
        <div class="flow-item">
            <div class="flow-item--image">
                <img src="images/rocket.svg">
            </div>
            <span class="flow-item--title">
                Доставка в течение <br>
                60-90 минут
            </span>
        </div>
        <div class="flow-item">
            <div class="flow-item--image">
                <img src="images/discount.svg">
            </div>
            <span class="flow-item--title">
                100% гарантии качества
            </span>
        </div>
        <div class="flow-item">
            <div class="flow-item--image">
                <img src="images/wallet.svg">
            </div>
            <span class="flow-item--title">
                Оплата курьеру
            </span>
        </div>
    </div>
</section>